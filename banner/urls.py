from django.conf.urls.defaults import *
from django.contrib import admin

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^(?P<url_hash>[\w=_\-]{1,50})/$', 'banner.views.click',
        name='banner-add'),
 )
