from banner.models import Banner
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect


def click(request, url_hash):
    banner = get_object_or_404(Banner, url_hash=url_hash)
    if banner.url and banner.click():
        return redirect(banner.url)
    raise Http404
