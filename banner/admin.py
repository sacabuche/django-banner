from django.contrib import admin
from banner.models import Banner


class BannerAdmin(admin.ModelAdmin):
    readonly_fields = ('clicks',)
    #prepopulated_fields = {"slug": ['name']}
    fieldsets = (
        (None, {
            'fields': (
                ('name', 'active'),
                ('url', 'url_active'),
                'swf',
                'image',
                ('max_clicks', 'clicks'),
                ('start_date_time', 'end_date_time'),
            ),
        }),
    )
    list_display = ('name', 'active', 'clicks', 'get_complete_url_hash')


admin.site.register(Banner, BannerAdmin)
