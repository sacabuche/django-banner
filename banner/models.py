#encoding=utf-8
from django.db import models
from django.contrib.sites.models import Site

from datetime import datetime
from base64 import urlsafe_b64encode


class Banner(models.Model):
    name = models.CharField(max_length=100, verbose_name='nom')
    url = models.URLField(blank=True, null=True)
    url_active = models.BooleanField('url active, oui ou non?', default=True)
    url_hash = models.CharField(max_length=80, unique=True, editable=False)
    swf = models.FileField(upload_to='banner/swf', blank=True)
    image = models.ImageField(upload_to='banner/photos')
    active = models.BooleanField('pub active, oui ou non?', default=True)
    clicks = models.IntegerField('No. des fois cliqué', default=0,
                                 editable=False)
    max_clicks = models.IntegerField(default=0, help_text='0 represent numero\
                                     ilimité des cliks')
    start_date_time = models.DateTimeField('Debut de campaing',
                                           blank=True, null=True)
    end_date_time = models.DateTimeField('Fin de campaign',
                                         blank=True, null=True)

    class Meta:
        verbose_name = 'Publicité'
        verbose_name_plural = verbose_name

    def __unicode__(self):
        return self.name

    def save(self):
        """Save and create url_hash"""
        if not self.id:
            super(Banner, self).save()
            self.__generate_url_hash()
        super(Banner, self).save()

    def __generate_url_hash(self):
        #hashed = hashlib.sha512(str(self.id)).digest()
        self.url_hash = urlsafe_b64encode(str(self.id))

    @models.permalink
    def get_url_hash(self):
        return ('banner-add', (self.url_hash,))

    def get_complete_url_hash(self):
        path = self.get_url_hash()
        domain = Site.objects.get_current().domain
        return 'http://%s%s' % (domain, path)

    def click(self):
        is_valid = self.is_valid()
        if is_valid:
            self.clicks += 1
        else:
            self.active = False
        super(Banner, self).save()
        return is_valid

    def is_valid(self):
        if not self.has_clicks() or not self.is_on_time():
            self.active = False
            super(Banner, self).save()
            return False
        return True

    def is_on_time(self):
        """
        Validate if is in the correct time to click
        """
        current_date_time = datetime.now()

        #validate if not self.start....
        try:
            is_gt_start = bool(current_date_time >= self.start_date_time)
        except TypeError:
            is_gt_start = True

        #validate if not self.end...
        try:
            is_lt_end = bool(current_date_time <= self.end_date_time)
        except TypeError:
            is_lt_end = True

        if is_gt_start and is_lt_end:
            return True
        return False

    def has_clicks(self):
        """
        Validate if max_click amount is not yet clicked
        """
        if self.max_clicks == 0:
            return True

        if self.clicks < self.max_clicks:
            return True
        return False

    def clear_clicks(self):
        self.clicks = 0
        self.save()
