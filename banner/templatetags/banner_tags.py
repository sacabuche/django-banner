from django import template
from django.conf import settings
from banner.models import Banner

register = template.Library()


WIDTH = getattr(settings, 'BANNER_WIDTH', 800)
HEIGHT = getattr(settings, 'BANNER_HEIGHT', 200)

def banneradd(context):
    #filter active banners
    active_banners = Banner.objects.filter(active=True).order_by('?')

    for banner in active_banners:
        if banner.is_valid():
            return {'banner':banner,
                    'HEIGHT': HEIGHT,
                    'WIDTH': WIDTH,
                    'SIZE': '%sx%s'%(WIDTH, HEIGHT),
                   }

    return

register.inclusion_tag('banner/banner.html',takes_context=True)(banneradd)
